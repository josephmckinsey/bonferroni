#!/bin/bash

echo 'Compiling LaTeX'
pdflatex main.tex
echo 'Pandocing the hell out of it'
pandoc -f latex -t rst main.tex -s -o main.rst --template rst.template
echo 'pandoc hates align*'
sed -i 's/aligned/align*/' main.rst
echo 'But we also want html'
rst2html.py main.rst > main.html
