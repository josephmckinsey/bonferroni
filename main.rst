=========================================
Some Notes on the Bonferroni Inequalities
=========================================

:tags: math, probability
:slug: bonferroni-inequalities
:summary: Some Notes on the Bonferroni Inequality
:date: 03-24-2020
:author: Joseph McKinsey
:modified: 03-24-2020 13:49

Introduction
============

Let :math:`E_1, \ldots, E_n` be a list of events in a probability space
with sample space :math:`\Omega`. Most are aware of the result

.. math:: P(E_1 \cup \cdots \cup E_n) \leq P(E_1) + \cdots + P(E_n).

Fewer are aware of the principle of inclusion-exclusion. If you aren’t,
don’t fear—all will be revealed with the power of indicator functions.

Indicator Functions
===================

**Indicator Function**: Let :math:`X \subseteq Y`, then
:math:`1_X : Y \to \{0, 1\}` is the indicator function on :math:`X`
assigning :math:`1` to elements in :math:`X` and :math:`0` elsewhere.

We can now rephrase the probability of an event :math:`E` as
:math:`\mathbb{E}[1_{E_1}]`. With a probability (density of mass)
function of :math:`p(x)`, we can interpret it in two ways. In terms of
integration, this is
:math:`\int_\Omega 1_{E_1} p(x) \, \mathrm{d}x = \int_{E_1} p(x)
\, \mathrm{d}x`. In terms of summations, this just becomes
:math:`\sum_{x \in E_1} p(x)`. With general measure theory, one defines
the integral accordingly :math:`\int_\Omega 1_{E_1} \, \mathrm{d}
\mu = \mu(E_1) = P(E_1)`. More importantly, indicator functions allow
one to prove many things in more general settings using the properties
of expectation.

**Markov’s Inequality**: Let :math:`X` be a non-negative random variable.

.. math:: \frac{\mathbb{E}[x]}{\lambda} \geq P(X \geq \lambda)

Our event is :math:`X \geq \lambda`. Then note
:math:`X \geq \lambda 1_{X \geq \lambda}`. To see this is true, consider
all the possible values :math:`X` could be. If :math:`X < \lambda`, then
our indicator function is :math:`0`, so :math:`X \geq 0` is the trivial
statement. If :math:`X \geq \lambda`, then the indicator function is
:math:`1`, so we have :math:`X \geq \lambda`. This is again trivially
true. Taking expectation of both sides. We can use the linearity of
expectation to get
:math:`\mathbb{E}[X] \geq \lambda E[1_{X \geq \lambda}]` or
:math:`\frac{\mathbb{E}[X]}{\lambda} \geq P(X \geq \lambda)`.

Principle of Inclusion-Exclusion
================================

The principle of inclusion-exclusion is usually framed as a way to count
a union, which is easier to understand.

As Counting
-----------

Say you are trying to count the size of a set
:math:`E_1 \cup \dots \cup E_n`. This might be computationally
expensive, but it might be much simpler to count the size of
intersections like :math:`E_{i_1} \cap \cdots \cap E_{i_m}`. We can
recover the size of the unions easily. First, we’ll tackle the
:math:`n = 3` case.

We start by counting all the elements in :math:`E_1`, :math:`E_2`, and
:math:`E_3`. But since these sets might intersect, we need to remove
duplicates: :math:`E_1 \cap E_2`, :math:`E_1
\cap E_3`, and :math:`E_2 \cap E_3`. But this might remove too many,
because these sets might intersect. If any two intersect, we know that
element was contained in every set, so we added it :math:`3` times and
removed it :math:`3` times, so we only need to add it back in once.

In the arbitrary case, we do the same thing. We add in sets, then
subtract intersections, add in more intersections, subtract, etc.

As Indicator Functions
----------------------

The counting approach has a few problems:

#. It’s hard to prove.

#. It’s hard to generalize.

We can rephrase and prove the principle with indicator functions. We
start with the easy fact that
:math:`1_{E_1} \cdots 1_{E_n} = 1_{E_1 \cap \cdots \cap E_n}`. Then let
:math:`E = E_1 \cap \cdots \cap E_n`, so
:math:`1_E \cdot 1_{E_i} = 1_{E_i}`. Lastly, we’ll need a little bit of
notation.

**:math:`A` and :math:`E_A`**: Let :math:`A` be a subset of
:math:`\{1, \ldots, n\}`. Now let :math:`E_A = \bigcap_{i \in A} E_i`.

Then our adding and subtracting can easily be represented with sums over
subsets :math:`A` of a specific size:

**Principle of Inclusion-Exclusion**

.. math::

   1_E = \sum_{|A| = 1} 1_{E_A} - \sum_{|A| = 2} 1_{E_A} + \cdots - (-1)^n
     \sum_{|A| = n} 1_{E_A}

We start with the statement
:math:`(1_E - 1_{E_1}) \cdots (1_E - 1_{E_n}) = 0`. Recall that the left
side is a function on :math:`\Omega`, so plugging in :math:`x \in 1_E`
tells us that :math:`x \in E`, so :math:`x \in E_i` for some :math:`i`.
Thus :math:`1_E - 1_{E_i} = 0`. If :math:`x \not\in E`, then
:math:`x \not\in E_i` for any :math:`i`, so :math:`1_E - 1_{E_i} = 0`.
Thus for all :math:`x \in \Omega`,
:math:`(1_E - 1_{E_1}) \cdots (1_E - 1_{E_n})(x) = 0(x) = 0`.

Now we can apply just expand our polynomial and get:

.. math:: 1_E^n - 1_E^{n-1} (1_{E_1} + \cdots + 1_{E_n}) + \cdots - \cdots + (-1)^n 1_{E_1} \cdots 1_{E_n} = 0

One subtraction later and a little sum notation, and we’ve arrived at
our answer.

.. math:: 1_E^n = \sum_{|A| = 1} 1_{E_A} - \cdots - (-1)^n \sum_{|A| = n} 1_{E_A}

By summing over both sides over :math:`x \in X`, we get

**Counting Version**

.. math::

   |E_1 \cup \cdots \cap E_n| = |E_1| + \cdots + |E_n| - |E_1 \cap E_2| - \cdots
     + \cdots - (-1)^n |E_1 \cap \cdots \cap E_n|.

Now we take the expectation as well to get

**Probability Version**

.. math::

   P(E_1 \cup \cdots \cap E_n) = P(E_1) + \cdots + P(E_n) - P(E_1 \cap E_2) - \cdots
     + \cdots - (-1)^n P(E_1 \cap \cdots \cap E_n).

But even better than this is that we can reason about the indicator’s
functions values with combinatorics.

Leading up to the Bonferroni Inequalities
=========================================

Let’s say we truncated our expanded polynomial from earlier at :math:`k`
terms, giving us

.. math:: f = 1_E - \sum_{|A| = 1} 1_{E_A} + \cdots + (-1)^k \sum_{|A| = k} 1_{E_A}.

This is not going to be :math:`0` anymore. If we choose :math:`k = 1`,
then anything in the intersection will be :math:`(-1)^k`.

To evaluate this more easily, notice that for any given :math:`x \in X`,
each term of :math:`f(x)` only depends on how many of
:math:`E_1, \cdots E_n` that :math:`x` is in. By summing over all
different combinations, we lose information about which subset that
:math:`x` was in.

Let :math:`B` be all the :math:`i` such that :math:`x \in E_i`.
Examining the term :math:`\sum_{|A| =
i} 1_{E_A}(x)`, each :math:`1_{E_A}` is :math:`1` iff
:math:`A \subset B`. If :math:`A \not\subseteq B`, one element of
:math:`A` has :math:`x \not\in i`, so :math:`x \not\in E_A`. If
:math:`A \subset B`, then :math:`x \in E_A`, so the indicator function
holds. Thus :math:`\sum_{|A| = i} 1_{E_A}` is precisely the number of
subsets of size :math:`i` contained in :math:`B`. If :math:`|B| = m`,
then this term is :math:`\binom{m}{i}`.

So then for :math:`x` in precisely :math:`m` :math:`E_i`,

.. math:: f(x) = \binom{m}{0} - \binom{m}{1} + \cdots + (-1)^k \binom{m}{k}.

There is the slight caveat that once :math:`k > m`, :math:`x` is not in
any intersection of :math:`k` subsets. So we take
:math:`\binom{m}{k} = 0` when :math:`k > m`. This just means that adding
extra terms doesn’t really improve the situation. From now on, we will
assume :math:`k \leq m`.

Counting with Inversions
------------------------

We will find a closed form expression of :math:`f` by pairing adjacent
terms in such a way that they cancel each other out. We’ll need a
perculiar function first.

Let
:math:`i : \mathcal{P}(\{1, \ldots, m\}) \to \mathcal{P}(\{1, \cdots, m\})`
where :math:`\mathcal{P}(S)` is the power-set of :math:`S`. We define
:math:`i` as follows:

.. math::

   i(A) = \begin{cases}
         A \cup \{m\} & \text{if } m \not\in A \\
     A - \{m\} & \text{if } m \in A
     \end{cases}.

Since :math:`i^2(A)` first adds :math:`n` then subtracts :math:`n` if
:math:`n \not\in A`, or subtracts :math:`n` then adds :math:`n` if
:math:`n \in A`, :math:`i^2(A) = A`. This fact is why we calll :math:`i`
an inversion. It also implies that :math:`i` is bijective.

But the really crucial property is that :math:`i` takes subsets of
:math:`\{1, \ldots, m\}` from one term of :math:`f` to another. A term
of :math:`f(x)`, say :math:`\binom{m}{i}`, counts the number of subsets
of a given size. Since :math:`i(A)` has either one more or one less
element of :math:`A`, counting the subset associated to :math:`i(A)`
cancels with the counting the subset :math:`A` since adjacent terms are
off by a :math:`-1`.

There is one exception. When we are looking at the subsets counted in
the last term, :math:`\binom{m}{k}`, we may try to pair it with subsets
that are one larger. Sadly those subsets are not being counted in
:math:`f`. Luckily we can still count them. They are all the subsets not
containing :math:`n` but still of size :math:`k`. In other terms,

.. math:: f(x) = (-1)^k \binom{m-1}{k}.

The Bonferroni Inequalities
===========================

Now that we have done all the counting inversion business, we really
only need the sign:

.. math::

   f = 1_E - \sum_{|A| = 1} 1_{E_A} + \cdots + (-1)^k \sum_{|A| = k}
     1_{E_A} \begin{cases}
     \geq 0 & \text{if } k \text{ is even} \\
     \leq 0 & \text{if } k \text{ is odd} \\
     \end{cases}.

This gives us

.. math::

   \begin{cases}
     1_E \geq \sum_{|A| = 1} 1_{E_A} - \cdots - (-1)^k \sum_{|A| = k} 1_{E_A} 
     & \text{if } k \text{ is even} \\
     1_E \leq \sum_{|A| = 1} 1_{E_A} - \cdots - (-1)^k \sum_{|A| = k} 1_{E_A} 
     & \text{if } k \text{ is odd} \\
    \end{cases}

Finally taking the expectation of both sides gives us

**Bonferroni Inequalities**

.. math::

   \begin{cases}
     P(E) \geq \sum_{|A| = 1} P(E_A) - \cdots - (-1)^k \sum_{|A| = k} P(E_A)
     & \text{if } k \text{ is even} \\
     P(E) \leq \sum_{|A| = 1} P(E_A) - \cdots - (-1)^k \sum_{|A| = k} P(E_A)
     & \text{if } k \text{ is odd} \\
    \end{cases}.

As a special case, we have

.. math:: P(E_1 \cup \cdots \cup E_n) \leq P(E_1) + \cdots + P(E_N).

None of this is original
========================

This end result is an early exercise in Tao & Vu’s book Additive
Combinatorics. It’s pretty obvious that it’s true, but figuring out a
proof took me down this path.

See these resources:

#. http://math.bme.hu/~gabor/oktatas/SztoM/TaoVu.AddComb.pdf for the
   original problem

#. https://planetmath.org/proofofbonferroniinequalities for an alternate
   approach that I found confusing

#. https://en.wikipedia.org/wiki/Inclusion%E2%80%93exclusion_principle
   to dig through for more about the principle of inclusion-exclusion

#. https://math.stackexchange.com/questions/208766/how-to-prove-bonferroni-inequalities
   which led me to

#. https://planetmath.org/proofofbonferroniinequalities for an alternate
   approach that I found confusing

#. https://math.stackexchange.com/questions/306771/combinatorial-proof-of-combinatorial-identity-involving-1k-binom-n-1k
   where I got the nice inversion trick
